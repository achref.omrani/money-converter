# Todo
* Les améliorations
  * Séparer le traitment
  * Ajouter des contol sur la saisie 
  * Personnaliser le thème
  * Améliorer le design de la page
  * Implementer toutes les règles
  * Ajouter des tests

* Les raccourcis
  * Utiliser un bouton à la place du switch pour inverser EUR/USD
  * Se baser sur un thème prédéfini de matériel
  * Mettre tout le traitement dans un seul composant    
