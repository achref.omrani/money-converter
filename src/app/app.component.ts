import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';

export interface ConvertionHistoryElement {
  realRate: number;
  selectedRate: number;
  initialValue: number;
  initialCurrency: string;
  calculatedValue: number;
  calculatedCurrency: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  max = 0.05;
  min = -0.05;
  tick = 3000;
  displayedColumns: string[] = [
    'realRate',
    'selectedRate',
    'initialValue',
    'calculatedValue',
  ];

  exchangeRate: number = 1.1;
  fromCurrency: string = 'EUR';
  toCurrency: string = 'USD';
  amountToConvert: number = 1;
  convertedAmount: number = 0;
  fixedExchangeRate!: number;
  convertionHistory: ConvertionHistoryElement[] = [];

  @ViewChild(MatTable) table!: MatTable<ConvertionHistoryElement>;

  ngOnInit() {
    this.calculeConvertedAmount();
    this.updateExchangeRate();
  }

  updateExchangeRate() {
    setInterval(() => {
      this.exchangeRate += this.getRandomValueInRange(this.min, this.max);
      this.convertedAmount = this.calculeConvertedAmount();
      this.convertionHistory.unshift({
        realRate: this.exchangeRate,
        selectedRate: this.fixedExchangeRate || this.exchangeRate,
        initialValue: this.amountToConvert,
        initialCurrency: this.fromCurrency,
        calculatedValue: this.convertedAmount,
        calculatedCurrency: this.toCurrency,
      });

      if (this.convertionHistory.length > 5) {
        this.convertionHistory.pop();
      }
      this.table.renderRows();
    }, this.tick);
  }

  calculeConvertedAmount() {
    return this.fixedExchangeRate == null
      ? this.amountToConvert * this.exchangeRate
      : this.amountToConvert * this.fixedExchangeRate;
  }

  switchCurrency() {
    let tmp = this.fromCurrency;
    this.fromCurrency = this.toCurrency;
    this.toCurrency = tmp;
  }

  getSymbol(value: string) {
    return value === 'EUR' ? '€' : '$';
  }

  private getRandomValueInRange(min: number, max: number) {
    return Math.random() * (max - min) + min;
  }
}
